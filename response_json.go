package osin

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
)

// OutputJSON encodes the Response to JSON and writes to the http.ResponseWriter
func OutputJSON(rs *Response, ctx *fasthttp.RequestCtx) error {
	// Add headers
	for i, k := range rs.Headers {
		for _, v := range k {
			ctx.Response.Header.Add(i, v)
		}
	}

	if rs.Type == REDIRECT {
		// Output redirect with parameters
		u, err := rs.GetRedirectUrl()
		if err != nil {
			return err
		}
		ctx.Response.Header.Add("Location", u)
		ctx.Response.Header.SetStatusCode(302)
	} else {
		// set content type if the response doesn't already have one associated with it
		if string(ctx.Response.Header.Peek("Content-Type")) == "" {
			ctx.Response.Header.Set("Content-Type", "application/json")
		}
		ctx.Response.Header.SetStatusCode(rs.StatusCode)

		encoder := json.NewEncoder(ctx.Response.BodyWriter())
		err := encoder.Encode(rs.Output)
		if err != nil {
			return err
		}
	}
	return nil
}
